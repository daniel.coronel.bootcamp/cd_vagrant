## All the resources to do this homework

Disable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All

bcdedit /set hypervisorlaunchtype auto

https://alchemist.digital/articles/vagrant-ansible-and-virtualbox-on-wsl-windows-subsystem-for-linux/

https://www.the-digital-life.com/automate-ansible-vagrant/

\\wsl$

C:\ProgramData\Docker\config

https://www.youtube.com/watch?v=BxM2zygCGOU

https://www.ntweekly.com/2019/09/20/how-to-change-docker-storage-data-folder-on-windows-server-2016/

https://www.freecodecamp.org/news/where-are-docker-images-stored-docker-container-paths-explained/

https://evodify.com/change-docker-storage-location/

## Commands

vagrant init hashicorp/bionic64
vagrant up
vagrant ssh
vagrant destroy
vagrant box list
vagrant box add


fdisk -l

fdisk /dev/sdb

mkfs -t ext4 /dev/sdb1

sudo lsblk
